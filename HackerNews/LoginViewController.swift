//
//  ViewController.swift
//  HackerNews
//
//  Created by Aahir Giri on 13/09/17.
//  Copyright © 2017 Aahir. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

class LoginViewController: UIViewController, GIDSignInUIDelegate {

    let defaults = UserDefaults.standard
    let mainStoryboard: UIStoryboard = UIStoryboard(name:"Main", bundle: nil)
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.nextVC), name: NSNotification.Name(rawValue: "LoginSuccess"), object: nil)
        if defaults.bool(forKey: "loggedIn") == true {
            nextVCWithoutAnimation()
        }
        self.navigationController?.navigationBar.isHidden = true
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    func nextVC() {
        let homeVC = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    func nextVCWithoutAnimation() {
        
        let homeVC = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(homeVC, animated: false)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func signInButton(_ sender: Any) {
        
        GIDSignIn.sharedInstance().signIn()
    }

}

