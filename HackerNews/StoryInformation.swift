//
//  StoryInformation.swift
//  HackerNews
//
//  Created by Aahir Giri on 14/09/17.
//  Copyright © 2017 Aahir. All rights reserved.
//

import Foundation
import RealmSwift

struct StoryInformation {
    let articleTitle: String
    let url: String?
    let submitter: String
    let score: Int
    let date: Int
    let id: Int
    let kids: [Int]?
}
struct Comments {
    let submitter: String?
    let date: Int?
    let text: String?
}
