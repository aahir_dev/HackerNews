//
//  CommentsTableViewCell.swift
//  HackerNews
//
//  Created by Aahir Giri on 14/09/17.
//  Copyright © 2017 Aahir. All rights reserved.
//

import UIKit

class CommentsTableViewCell: UITableViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var submitterLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(comment: Comments) {
        textView.isScrollEnabled = false
        let epochTime: TimeInterval = Double(comment.date!)
        let date = Date(timeIntervalSince1970: epochTime)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM, yyyy HH:mm"
        timeLabel.text = formatter.string(from: date)
        submitterLabel.text = comment.submitter
        textView.text = comment.text
 //       self.adjustUITextViewHeight(textView: textView)
    }
    
//    func adjustUITextViewHeight(textView : UITextView)
//    {
//        textView.translatesAutoresizingMaskIntoConstraints = true
//        textView.sizeToFit()
//        textView.isScrollEnabled = false
//    }

}
