//
//  HomeTableViewCell.swift
//  HackerNews
//
//  Created by Aahir Giri on 14/09/17.
//  Copyright © 2017 Aahir. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var voteLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var urlLabel: UILabel!
    @IBOutlet weak var submittedLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(story: StoryInformation) {
        let currentTime = Date().millisecondsSince1970/1000
        let timeDif = currentTime - story.date
        titleLabel.text = story.articleTitle
        if let kid = story.kids {
            commentLabel.text = String(describing: kid.count)
        }
        let hours: Int = Int(timeDif/3600)
        timeLabel.text = "\(hours) hours ago"
        urlLabel.text = story.url ?? ""
        submittedLabel.text = story.submitter
        voteLabel.text = "\(story.score)"
    }

}

extension Date {
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
}
