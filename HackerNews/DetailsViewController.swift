//
//  DetailsViewController.swift
//  HackerNews
//
//  Created by Aahir Giri on 14/09/17.
//  Copyright © 2017 Aahir. All rights reserved.
//

import UIKit
import Firebase

class DetailsViewController: UIViewController {

    @IBOutlet weak var articlesView: UIView!
    @IBOutlet weak var commentsView: UIView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var commentsButton: UIButton!
    @IBOutlet weak var articleButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var urlLabel: UILabel!
    @IBOutlet weak var submitterLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
 
    var firebase: Firebase!
    var mode = 0
    var story: StoryInformation?
    var commentsArray: [Comments] = []
    let url =  "https://hacker-news.firebaseio.com/v0/"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Story Details"
        if let url = story?.url {
            webView.loadRequest(URLRequest(url: URL(string: url)!))
        } else {
            webView.frame.size.height = 0
        }
        tableView.alpha = 1.0
        webView.alpha = 0.0
        commentsView.alpha = 1.0
        articlesView.alpha = 0.0
        titleLabel.text = story?.articleTitle
        urlLabel.text = story?.url ?? "NA"
        submitterLabel.text = story?.submitter
        let epochTime: TimeInterval = Double(story!.date)
        let date = Date(timeIntervalSince1970: epochTime)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM, yyyy"
        timeLabel.text = formatter.string(from: date)
        if let kid = story!.kids {
            let kidsString: String = String(describing: kid.count)
            let commentString: String? = kidsString.appending(" COMMENTS")
            if commentString != nil {
                commentsButton.setTitle(commentString, for: .normal)
            } else {
                commentsButton.setTitle("COMMENTS", for: .normal)
            }
        } else {
             commentsButton.setTitle("COMMENTS", for: .normal)
        }
        tableView.dataSource = self
        tableView.delegate = self
        if story!.kids != nil {
            getComments()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        firebase = Firebase(url: url)
     //   gettingStories = false
        commentsArray = []
 //       refreshControl = UIRefreshControl()
    }
    
    func getComments() {
        var storyMap = [Int:Comments]()
            for kidId in story!.kids! {
                let query = self.firebase.child(byAppendingPath: "item").child(byAppendingPath: String(kidId))
                query?.observeSingleEvent(of: .value, with: { snapshot in
                    storyMap[kidId] = self.getComments(snapshot!)
                    var sortedComments = [Comments]()
                    for kidId in self.story!.kids! {
                        if storyMap[kidId] != nil {
                            sortedComments.append(storyMap[kidId]!)
                        }
                    }
                    self.commentsArray = sortedComments
                    self.tableView.reloadData()
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }, withCancel: self.failed)
                
            }
    }
    
    func getComments(_ snapshot: FDataSnapshot) -> Comments {
        let data = snapshot.value as! Dictionary<String, Any>
        let text = data["text"] as? String
        let submitter = data["by"] as? String
        let date = data["time"] as? Int
        return Comments(submitter: submitter, date: date, text: text)
    }
    
    func failed(_ error: Error?) -> Void {
        self.commentsArray.removeAll()
        //    self.tableView.reloadData()
        //  self.showErrorMessage(self.FetchErrorMessage)
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    @IBAction func onPressArticle(_ sender: Any) {
        if mode == 0 {
            UIView.animate(withDuration: 0.25, animations: {
                self.commentsView.alpha = 0.0
                self.articlesView.alpha = 1.0
                self.tableView.alpha = 0.0
                self.webView.alpha = 1.0
            })
            mode = 1
        }
        
    }
    @IBAction func onPressComments(_ sender: Any) {
        if mode == 1 {
            UIView.animate(withDuration: 0.25, animations: {
                self.commentsView.alpha = 1.0
                self.articlesView.alpha = 0.0
                self.tableView.alpha = 1.0
                self.webView.alpha = 0.0
            })
        }
        mode = 0
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

        
    }
    
}

extension DetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let comment = commentsArray[indexPath.row]
        if let commentSize = (comment.text?.characters.count) {
            let lineCount = commentSize/57
            return CGFloat(lineCount * 10 + 10)
        }
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentsTableViewCell") as! CommentsTableViewCell
        let comment = commentsArray[indexPath.row]
        cell.configureCell(comment: comment)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
