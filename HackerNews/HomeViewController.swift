//
//  HomeViewController.swift
//  HackerNews
//
//  Created by Aahir Giri on 13/09/17.
//  Copyright © 2017 Aahir. All rights reserved.
//

import UIKit
import Firebase
import RealmSwift

class HomeViewController: UIViewController {

    let url = "https://hacker-news.firebaseio.com/v0/"
    let storyLimit: UInt = 30
    var firebase: Firebase!
    var storiesArray: [StoryInformation] = []
    var refreshControl: UIRefreshControl!
    var gettingStories: Bool!
    
    let realm = try! Realm()
 //   lazy var storyx: StoryInformation = { self.realm.objects(StoryInformation) }()
    
    @IBOutlet weak var tableView: UITableView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        firebase = Firebase(url: url)
        gettingStories = false
        storiesArray = []
        refreshControl = UIRefreshControl()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getStories()
        tableView.dataSource = self
        tableView.delegate = self
        self.title = "Top Stories"
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.isHidden = false
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func getStories() {
        gettingStories = true
        var storyMap = [Int:StoryInformation]()
        
        let query = firebase.child(byAppendingPath: "topstories").queryLimited(toFirst: storyLimit)
        query?.observeSingleEvent(of: .value, with: { snapshot in
            let storyIds = snapshot?.value as! [Int]
            
            for storyId in storyIds {
                let query = self.firebase.child(byAppendingPath: "item").child(byAppendingPath: String(storyId))
                query?.observeSingleEvent(of: .value, with: { snapshot in
                    storyMap[storyId] = self.getStory(snapshot!)
                    
                    if storyMap.count == Int(self.storyLimit) {
                        var sortedStories = [StoryInformation]()
                        for storyId in storyIds {
                            sortedStories.append(storyMap[storyId]!)
//                            try! realm.write {
//                                realm.add(storyMap[storyId])
//                            }
                        }
                        self.storiesArray = sortedStories
                        self.tableView.reloadData()
                        self.refreshControl.endRefreshing()
                        self.gettingStories = false
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
                }, withCancel: self.failed)
            }
        }, withCancel: self.failed)
    }
    
    func getStory(_ snapshot: FDataSnapshot) -> StoryInformation {
        let data = snapshot.value as! Dictionary<String, Any>
        let articleTitle = data["title"] as! String
        let url = data["url"] as? String
        let submitter = data["by"] as! String
        let id = data["id"] as! Int
        let kids = data["kids"] as? [Int]
        let score = data["score"] as! Int
        let date = data["time"] as! Int
        return StoryInformation(articleTitle: articleTitle, url: url, submitter: submitter, score: score, date: date, id: id, kids: kids)
    }
    
    func failed(_ error: Error?) -> Void {
        self.gettingStories = false
        self.storiesArray.removeAll()
        self.tableView.reloadData()
      //  self.showErrorMessage(self.FetchErrorMessage)
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return storiesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let story = storiesArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
        cell.configureCell(story: story)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let story = storiesArray[indexPath.row]
        let mainStoryboard: UIStoryboard = UIStoryboard(name:"Main", bundle: nil)
        let detailsVC = mainStoryboard.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
        detailsVC.story = story
        self.navigationController?.pushViewController(detailsVC, animated: true)

    }
}
